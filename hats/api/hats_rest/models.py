from django.db import models

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=255)
    import_href = models.CharField(max_length=200, unique=True)

class Hat(models.Model):
    fabric = models.CharField(max_length=255)
    style_name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name = "hats",
        on_delete=models.CASCADE
    )
