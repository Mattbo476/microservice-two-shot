from django.urls import path
from .views import hat_list

urlpatterns = [
    path("hats/", hat_list, name = "hats_list")
]
