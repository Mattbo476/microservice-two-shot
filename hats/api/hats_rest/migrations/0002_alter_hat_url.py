from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hat',
            name='url',
            field=models.URLField(null=True),
        ),
    ]
