from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LocationVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=255)),
                ('import_href', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='Hat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fabric', models.CharField(max_length=255)),
                ('style_name', models.CharField(max_length=255)),
                ('color', models.CharField(max_length=255)),
                ('url', models.URLField()),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hats_rest.locationvo')),
            ],
        ),
    ]
