from django.http import JsonResponse
from .models import Hat, LocationVO
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'closet_name',
        'import_href',
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "location",
    ]
    encoders= {
        "location": LocationVOEncoder(),
    }

class HatDetailEncoder:
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "location"
    ]

@require_http_methods(["GET", "POST"])
def hat_list(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)
        print(content)
        location_vo = LocationVO.objects.get(import_href=content["location"])
        content["location"] = location_vo
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder= HatListEncoder,
            safe=False,
        )
