from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO

# Encoders

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "href",]
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin", "id"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin", "id"]


# Create your views here.
@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,)
    else: 
        content = json.loads(request.body)
        try: 
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Bin does not exist"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    
@require_http_methods(["GET", "DELETE", "PUT"])
def shoe_detail(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            {"shoes":shoes},
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _= Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: 
        content = json.loads(request.body)
        try:
            Shoe.objects.filter(id=id).update(**content)
            shoes= Shoe.objects.filter(id=id).delete()
            return JsonResponse(
                shoes, 
                encoder=ShoeListEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe id"},
                status=400)

            

