# Wardrobify

Team:

* Kane Rodriguez - Shoes
* Matt - Which microservice? => Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

First I installed the Hats app. I started by making a LocationVO and Hats model. Then I made a polling connection between my microservice's LocationVO models and the wardrobe's Location models. I then created all the views and registered them under the correct URLs. I started on the React components to make a hats list that displayed all hats. Then, I created a form to make a new hat. I registered the components under the correct routes and links on the Apps/Nav files. At the end, I made a delete button for each hat object in the attendee list that will re-render the component once a deletion occurs.
